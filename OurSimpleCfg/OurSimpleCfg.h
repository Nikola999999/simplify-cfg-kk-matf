//
// Created by neske on 23.4.23..
//

#ifndef LLVM_PROJECT_OURSIMPLECFG_H
#define LLVM_PROJECT_OURSIMPLECFG_H

#include "llvm/IR/CFG.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Support/raw_ostream.h"

#include<iostream>
#include<vector>
#include<set>
#include <unordered_map>

using namespace llvm;

class OurSimpleCfg {
public:

void createGraph(Function&F);
void iterativelyEliminateUnreachableBasicBlocks();

void mergeSimpleBlocks();


private:
    void addEdge(BasicBlock* from ,BasicBlock*to);
    void removeBasicBlock(BasicBlock*);

    bool eliminateUnreachableBasicBlocks();

    bool merge2SimpleBlocks();




    std::unordered_map<BasicBlock*,std::set<BasicBlock*>>adjacency_list;
    std::unordered_map<BasicBlock*,std::set<BasicBlock*>>reverse_adjacency_list;
    BasicBlock*initialBasicBlock;

};


#endif //LLVM_PROJECT_OURSIMPLECFG_H
