#include<iostream>

#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "OurSimpleCfg.h"
using namespace llvm;





namespace {
  // Hello - The first implementation, without getAnalysisUsage.
  struct OurSimpleCfgPass : public FunctionPass {
    static char ID; // Pass identification, replacement for typeid
    OurSimpleCfgPass() : FunctionPass(ID) {}

    bool runOnFunction(Function &F) override {

//        auto BBB=F.begin();
//        BasicBlock* BB=&*BBB;
//
//        auto instructionBegin=BB->rbegin();
//        auto insturctionsEnd=BB->rend();
//
//        BB->eraseFromParent();




        OurSimpleCfg *CFG=new OurSimpleCfg();
        CFG->createGraph(F);
//        CFG->mergeSimpleBlocks();
      CFG->iterativelyEliminateUnreachableBasicBlocks();
        std::cout <<"something done"<<std::endl;
        delete CFG;

//        auto BasicBlockBegin=F.begin();
//        auto BasicBlockEnd=F.end();
//        errs()<<"**************************\n";
//        while(BasicBlockBegin!=BasicBlockEnd){
//            auto instructionBegin=BasicBlockBegin->begin();
//            auto instructionEnd=BasicBlockBegin->end();
//
//            while(instructionBegin!=instructionEnd){
//                errs()<<instructionBegin->getOpcodeName()<<"\n";
//                instructionBegin++;
//            }
//            errs()<<"\n\n";
//            BasicBlockBegin++;
//        }
//        errs()<<"*******************************\n";
//
//
//        std::cout<<"-------------"<<std::endl;
        return true;
    }
  };
}

char OurSimpleCfgPass::ID = 0;
static RegisterPass<OurSimpleCfgPass> X("simplecfg", "simple cfg pass", false,false);

