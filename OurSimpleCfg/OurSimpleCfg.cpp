//
// Created by neske on 23.4.23..
//



#include "OurSimpleCfg.h"
#include "llvm/IR/BasicBlock.h"

void OurSimpleCfg::addEdge(llvm::BasicBlock *from, llvm::BasicBlock *to) {
    adjacency_list[from].insert(to);
    reverse_adjacency_list[to].insert(from);
}

void OurSimpleCfg::removeBasicBlock(llvm::BasicBlock *toRemove) {
    for(BasicBlock* succesors:adjacency_list[toRemove]){
        reverse_adjacency_list[succesors].erase(toRemove);
    }

    for(BasicBlock* predecesors:reverse_adjacency_list[toRemove]){
        adjacency_list[predecesors].erase(toRemove);
    }
    adjacency_list.erase(toRemove);
    reverse_adjacency_list.erase(toRemove);

}

void OurSimpleCfg::createGraph(llvm::Function &F) {

    auto BasicBlocksBegin = F.begin();
    auto BasicBlocksEnd=F.end();

    initialBasicBlock=&*BasicBlocksBegin;

    while(BasicBlocksBegin!=BasicBlocksEnd){
        adjacency_list[&*BasicBlocksBegin]={};
        reverse_adjacency_list[&*BasicBlocksBegin]={};
        BasicBlocksBegin++;
    }
    BasicBlocksBegin=F.begin();


    while(BasicBlocksBegin != BasicBlocksEnd){

        for(auto Successor:successors(&*BasicBlocksBegin))
            addEdge(&*BasicBlocksBegin,Successor);
        BasicBlocksBegin++;
    }
}



bool OurSimpleCfg::eliminateUnreachableBasicBlocks() {

    std::set<BasicBlock*>unreachable;
    for(auto x:reverse_adjacency_list){

        BasicBlock*bb=x.first;
        if(bb==initialBasicBlock)
            continue;
       errs()<<x.second.size()<<"\n";

        if(x.second.empty() ){

            unreachable.insert(x.first);
        }
    }
    bool madeChange=!unreachable.empty();
    for(BasicBlock*bb:unreachable){
        removeBasicBlock(bb);
        bb->eraseFromParent();

    }
    return madeChange;
}

void OurSimpleCfg::iterativelyEliminateUnreachableBasicBlocks() {

    int i=0;

    while(eliminateUnreachableBasicBlocks()){
        std::cout<<++i<<std::endl;
        //let him cook
    }

}

void OurSimpleCfg::mergeSimpleBlocks() {

    while(merge2SimpleBlocks()){
        //let him cook
        ;
    }


}
bool OurSimpleCfg::merge2SimpleBlocks() {
    std::cout<<"allalal"<<std::endl;
    bool madeChange=false;
    BasicBlock*head= nullptr;
    BasicBlock*tail= nullptr;
    for(auto par:adjacency_list){
        if(par.second.empty())
            continue;
        if(par.second.size()==1){
            auto succesor=*(par.second.begin());
            if(succesor==par.first)
                continue;
            if(reverse_adjacency_list[succesor].size()==1){
                madeChange=true;
                head=par.first;
                tail=succesor;
                break;
            }
        }
    }
    if(madeChange){
        adjacency_list[head]=adjacency_list[tail];
        for(auto succesor:adjacency_list[tail]){
            reverse_adjacency_list[succesor].erase(tail);
            reverse_adjacency_list[succesor].insert(head);

        }
        removeBasicBlock(tail);

//        removeBasicBlock(tail);


        head->splice(head->getTerminator()->getIterator(),tail);

        errs()<<"getTerminator:"<<head->getTerminator()->getOpcodeName()<<"\n ";
        head->getTerminator()->eraseFromParent();
        tail->eraseFromParent();


        auto instructionBegin=head->begin();
        auto instructionEnd=head->end();

        while(instructionBegin!=instructionEnd){
            errs()<<instructionBegin->getOpcodeName()<<"\n";
            instructionBegin++;
        }

    }
    return madeChange;



}